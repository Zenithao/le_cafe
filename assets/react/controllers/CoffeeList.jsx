import React, { useEffect, useState } from 'react';
import Loader from './components/Loader';
import Search from './components/Search';
import Filter from './components/Filter';
import Card from './components/Card';

export default function (props) {
  const [loader, setLoader] = useState(true);
  const [data, setData] = useState([]);
  // nombre d'éléments Card à afficher
  const [nbrOfItems, setNbrOfItems] = useState(6);
  const [searchTerm, setSearchTerm] = useState("");
  const [user, setUser] = useState(props.user);
  const [favorites, setFavorites] = useState(props.favorites);

  // récupèrer les données de l'API
  const getData = async () => {
    const resp = await fetch('https://api.sampleapis.com/coffee/hot');
    const json = await resp.json();
    json.splice(json.length - 2, 2);
    setData(json);
    setLoader(false);
  }

  // formulaire de recherche
  const handleChangeTerm = (e) => {
    setSearchTerm(e.target.value);
  }

  const handleOrderAsc = () => {
    setData((newData) => [...newData.sort((a, b) => (a.title > b.title ? 1 : -1))]);
  }

  const handleOrderDesc = () => {
    setData((newData) => [...newData.sort((a, b) => (a.title < b.title ? 1 : -1))]);
  }

  const viewMore = (e) => {
    e.preventDefault();
    setNbrOfItems(prevNbrOfItems => prevNbrOfItems + 6);
  }

  const toggleFavorite = async (e, id) => {
    e.preventDefault();
    const resp = await fetch(`https://le-cafe.adrien-le-pober.com/ajout-aux-favoris/${id}/${user}`)
    const json = await resp.json();
    if (json.added) {
      setFavorites(prevFavorites => [...prevFavorites, id]);
    } else if (json.removed) {
      setFavorites(favorites.filter(item => item !== id));
    }
  }

  useEffect(() => {
    getData();
  }, []);

  return (
    <section className="my-3">
      {
        loader ?
          <Loader />
          :
          <>
            <div className="d-flex justify-content-between">
              <Filter handleOrderAsc={handleOrderAsc} handleOrderDesc={handleOrderDesc} />
              <Search handleChangeTerm={handleChangeTerm} />
            </div>
            <div className="d-flex justify-content-center flex-wrap">
              {Object.keys(data).filter(item => (
                data[item].title.toLowerCase().includes(searchTerm.toLowerCase())
              )).map((item, index) => (
                <Card
                  id={data[item].id}
                  key={data[item].id}
                  image={data[item].image}
                  title={data[item].title}
                  description={data[item].description}
                  style={index < nbrOfItems ? {display: 'flex'} : {display: 'none'}}
                  toggleFavorite={toggleFavorite}
                  favorites={favorites}
                  isOnline={user}
                />
              ))}
            </div>
            {
              data.length > nbrOfItems ?
                <div className="d-flex justify-content-end">
                  <a
                    href="#"
                    className="text-secondary me-3 me-md-5 me-lg-6 me-xl-8 me-xxl-10"
                    onClick={viewMore}
                  >
                    Afficher plus
                  </a>
                </div>
              :
              ''
            }

          </>
      }
    </section>
  )
}