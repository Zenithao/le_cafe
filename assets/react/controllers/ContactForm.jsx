import React, { useState } from "react";
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

export default function() {
  const [submitted, setSubmitted] = useState(false);

  const schema = yup.object().shape({
    firstname: yup
      .string("Le prénom doit être une chaîne de caractères")
      .max(50, "Désolé, votre prénom est trop long")
      .required("Votre prénom est requis"),
    lastname: yup
      .string("Le nom doit être une chaîne de caractères")
      .max(50, "Désolé, votre nom est trop long")
      .required("Votre nom est requis"),
    phone: yup
      .string()
      .matches(/^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/, 'Le format du numéro de téléphone est invalide'),
    email: yup.
      string("L'email doit être une chaîne de caractères")
      .max(255, "Désolé, votre adresse email est trop longue")
      .email("Le format email n'est pas respecté")
      .required("Votre adresse email est requise"),
    message: yup
      .string("Le message doit être une chaîne de caractères")
      .max(10000, "Le message est trop long, moins de 10000 caractères s'il vous plaît")
      .required("Le message est requis"),
  })

  const { register, handleSubmit, formState: {errors} } = useForm({
    resolver: yupResolver(schema)
  });

  const onSubmit = (data) => {
    setSubmitted(true);
    fetch('https://le-cafe.adrien-le-pober.com/validate/contact', {
      method: 'POST',
      body: JSON.stringify(data),
    })
    .then(res => res.json())
    .then((response) => {
      if (response.ok) {
        window.location.href = "https://le-cafe.adrien-le-pober.com/contact";
      }
      if (response.validationErrors.validationErrors) {
        setSubmitted(false);
      }
    })
  }

  return (
    <form 
      onSubmit={handleSubmit(onSubmit)} 
      className="d-flex flex-column align-items-center"
    >
      <p className="text-muted">Les champs marqués d'un <span className="text-danger">*</span> sont obligatoires.</p>
      <ul>
        {Object.keys(errors).map((field) => (
          <li className="text-danger" key={field}>{errors[field].message}</li>
        ))}
      </ul>
      <div className="form-group my-1 col-11 col-md-6 col-xxl-4">
        <label htmlFor="firstname">Prénom<span className="text-danger">*</span></label>
        <input 
          type="text"
          className="form-control"
          id="firstname"
          {...register("firstname")}
        />
      </div>
      <div className="form-group my-1 col-11 col-md-6 col-xxl-4">
        <label htmlFor="lastname">Nom</label>
        <input 
          type="text"
          className="form-control"
          id="lastname"
          {...register("lastname")}
        />
      </div>
      <div className="form-group my-1 col-11 col-md-6 col-xxl-4">
        <label htmlFor="phone">Téléphone</label>
        <input 
          type="text"
          className="form-control"
          id="phone"
          {...register("phone")}
        />
      </div>
      <div className="form-group my-1 col-11 col-md-6 col-xxl-4">
        <label htmlFor="email">Email</label>
        <input 
          type="email" 
          className="form-control" 
          id="email" 
          {...register("email")}
        />
      </div>
      <div className="form-group my-1 col-11 col-md-6 col-xxl-4">
        <label htmlFor="message">Message<span className="text-danger">*</span></label>
        <textarea
          rows="4"
          className="form-control" 
          id="message" 
          {...register("message")}
        />
      </div>
      <div>
        <input
          type="submit"
          className={'btn btn-secondary my-3 ' + (submitted ? 'disabled' : '')}
          value="Envoyer"
          role="button"
          aria-disabled={submitted}
        />
      </div>
    </form>
  )
}