import React from "react";

function Search(props) {
  return (
    <form action="" autoComplete="on" id="wrap" className="me-3 me-md-5 me-lg-6 me-xl-8 me-xxl-9">
      <i className="fa-regular fa-magnifying-glass text-secondary fa-xl search__icon"></i>
      <input
        id="search__field"
        name="search"
        type="text"
        placeholder="Que recherchez-vous ?"
        onChange={props.handleChangeTerm}
      />
    </form>
  )
}

export default Search;