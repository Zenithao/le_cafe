import React from "react";

function Loader() {
  return (
    <div className="d-flex min-vh-custom align-items-center">
      <div className="spinner-border mb-5 text-secondary" role="status">
        <span className="visually-hidden">Loading...</span>
      </div>
    </div>
  )
}

export default Loader;