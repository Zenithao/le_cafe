import React from "react";

function Card({id, image, title, description, style, toggleFavorite, favorites, isOnline}) {
  return (
    <div
      className="card border-secondary bg-primary shadow m-2 col-11 col-md-5 col-xl-3 m-xl-3"
      style={style}
    >
      <div className="ratio ratio-4x3">
        <img src={image} className="card-img-top card__img" alt={title} />
      </div>
      <div className="card-body position-relative text-light">
        <h5 className="card-title">{title}</h5>
        <p className="card-text pb-5">{description}</p>
        {isOnline ? 
          <a href="#" className="position-absolute bottom-0 end-0 p-3" onClick={(e) => toggleFavorite(e, id)}>
            <i className={'text-secondary fs-4 bi ' + (favorites.find(elem => elem === id) ? 'bi-heart-fill' : 'bi-heart') }></i>
          </a>
        : ''}

      </div>
    </div>
  )
}

export default Card;