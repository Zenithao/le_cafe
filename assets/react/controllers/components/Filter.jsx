import React from "react";

function Filter(props) {
  return (
    <div className="dropdown ms-3 ms-md-5 ms-lg-6 ms-xl-8 ms-xxl-9">
      <a className="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
        Filtres
      </a>

      <ul className="dropdown-menu" aria-labelledby="dropdownMenuLink">
        <li>
          <a 
            className="dropdown-item"
            href="#"
            onClick={props.handleOrderAsc}
          >
            Ordre croissant
          </a>
        </li>
        <li>
          <a
            className="dropdown-item"
            href="#"
            onClick={props.handleOrderDesc}
          >
            Ordre décroissant
          </a>
        </li>
      </ul>
    </div>
  )
}

export default Filter;