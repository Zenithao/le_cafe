import React, { useRef } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

export default function () {
  const formErrors = useRef(null);
  const schema = yup.object().shape({
    email: yup.
      string("L'email doit être une chaîne de caractères")
      .max(180, "Désolé, votre adresse email est trop longue")
      .email("Le format email n'est pas respecté")
      .required("Votre adresse email est requise"),
    plainPassword: yup
      .string("Le mot de passe doit être une chaîne de caractères")
      .min(6, "Au moins 6 caractères pour le mot de passe")
      .max(100, "Moins de 100 caractères pour le mot de passe")
      .required("Le mot de passe est requis"),
    rgpd: yup
      .bool()
      .oneOf([true], "Vous devez consentir au règlement général sur la protection des données (RGPD)")
  })

  const { register, handleSubmit, formState: {errors} } = useForm({
    resolver: yupResolver(schema)
  });

  const onSubmit = (data) => {
    fetch('https://le-cafe.adrien-le-pober.com/validate/user', {
      method: 'POST',
      body: JSON.stringify(data),
    })
    .then(res => res.json())
    .then((response) => {
      if (response.ok) {
        window.location.href = "https://le-cafe.adrien-le-pober.com/connexion";
      }
      if (response.formErrors.existingUser) {
        const existingUser = document.createElement("li");
        existingUser.innerText = response.formErrors.existingUser;
        formErrors.current.appendChild(existingUser);
      }
    })
  }

  return (
    <form 
      onSubmit={handleSubmit(onSubmit)} 
      className="d-flex flex-column align-items-center"
    >
      <ul ref={formErrors}>
        {Object.keys(errors).map((field) => (
          <li className="text-danger" key={field}>{errors[field].message}</li>
        ))}
      </ul>
      <div className="form-group my-1 col-11 col-md-6 col-xxl-4">
        <label htmlFor="email">Email</label>
        <input 
          type="email" 
          className="form-control" 
          id="email" 
          {...register("email")}
        />
      </div>
      <div className="form-group my-1 col-11 col-md-6 col-xxl-4">
        <label htmlFor="password">Mot de passe</label>
        <input 
          type="password" 
          className="form-control" 
          id="password" 
          {...register("plainPassword")}
        />
      </div>
      <div className="form-check my-1 col-11 col-md-6 col-xxl-4">
        <input 
          type="checkbox"
          className="form-check-input"
          id="rgpd" 
          {...register("rgpd")}
        />
        <label htmlFor="rgpd" className="form-check-label">
          J'accepte que mes données soient conservées
        </label>
      </div>
      <div>
        <input type="submit" className="btn btn-secondary my-3" value="Valider"/>
      </div>
    </form>
  )
}