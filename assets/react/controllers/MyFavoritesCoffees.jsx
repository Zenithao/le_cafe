import React, { useEffect, useState } from 'react';
import Loader from './components/Loader';
import Card from './components/Card';

function MyFavoritesCoffees(props) {
  const [loader, setLoader] = useState(true);
  const [data, setData] = useState([]);
  const [user, setUser] = useState(props.user);
  const [favorites, setFavorites] = useState(props.favorites);

  // récupèrer les données de l'API
  const getData = async () => {
    const resp = await fetch('https://api.sampleapis.com/coffee/hot');
    const json = await resp.json();
    json.splice(json.length - 2, 2);
    setData(json);
    setLoader(false);
  }

  const toggleFavorite = async (e, id) => {
    e.preventDefault();
    const resp = await fetch(`https://le-cafe.adrien-le-pober.com/ajout-aux-favoris/${id}/${user}`)
    const json = await resp.json();
    if (json.added) {
      setFavorites(prevFavorites => [...prevFavorites, id]);
    } else if (json.removed) {
      setFavorites(favorites.filter(item => item !== id));
    }
  }

  useEffect(() => {
    getData();
  }, []);

  return (
    <section className="my-3">
      {
        loader ?
          <Loader />
          :
          favorites.length > 0 ?
            <div className="d-flex justify-content-center flex-wrap">
              {Object.keys(data).filter(item => (
                favorites.find(elem => elem == data[item].id)
              )).map((item) => (
                <Card
                  id={data[item].id}
                  key={data[item].id}
                  image={data[item].image}
                  title={data[item].title}
                  description={data[item].description}
                  style={{display: 'flex'}}
                  toggleFavorite={toggleFavorite}
                  favorites={favorites}
                  isOnline={user}
                />
              ))}
            </div>
          :
          <p>Vous n'avez pas encore ajouté de favoris</p>
      }
    </section>
  )
}

export default MyFavoritesCoffees;