const $ = require("jquery");
require("slick-carousel");

$("#slider").slick({
  centerMode: true,
  autoplay: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  prevArrow:
    '<span class="prev_arrow"><i class="fas fa-angle-left"></i></span>',
  nextArrow:
    '<span class="next_arrow"><i class="fas fa-angle-right"></i></span>',
  responsive: [
    {
      breakpoint: 1400,
      settings: {
        slidesToShow: 4,
      },
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
      },
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        arrows: false,
      },
    },
  ],
});
