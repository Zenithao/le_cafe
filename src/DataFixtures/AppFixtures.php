<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @codeCoverageIgnore
 */
class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {

        $user = (new User())
            ->setEmail('user@test.com')
            ->setUsername('user')
            ->setRoles(['ROLE_USER'])
            ->setFavorites([1])
            ->setIsVerified(true);

        $password = $this->hasher->hashPassword($user, 'user123');
        $user->setPassword($password);

        $manager->persist($user);

        $manager->flush();
    }
}
