<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CoffeeController extends AbstractController
{
    #[Route('/les-cafes', name: 'app_coffees')]
    public function coffeeList(): Response
    {
        return $this->render('coffee/index.html.twig');
    }

    #[Route('/mes-cafes', name: 'app_my_coffees')]
    public function myCoffeeList(): Response
    {
        return $this->render('coffee/favorites.html.twig');
    }

    #[Route('/ajout-aux-favoris/{id}/{user}', name: 'add_to_favorites', methods: ["GET","POST"])]
    #[IsGranted('ROLE_USER')]
    public function addToFavorites(
        int $id,
        int $user,
        UserRepository $userRepository
    ): JsonResponse {
        /** @var User $user */
        $user = $userRepository->find($user);

        if ($user) {
            $favorites = $user->getFavorites();
            if (false === array_search($id, $favorites)) {
                $favorites[] = $id;
                $user->setFavorites($favorites);
                $userRepository->save($user, true);
    
                return new JsonResponse(['added' => 'added']);
            } else {
                $newFavorites = [];
                foreach ($favorites as $favorite) {
                    if ($favorite !== $id) {
                        $newFavorites[] = $favorite;
                    }
                }

                $user->setFavorites($newFavorites);
                $userRepository->save($user, true);
    
                return new JsonResponse(['removed' => 'removed']);
            }
        }

        return new JsonResponse(['error' => 'une erreur est survenue']);
    }
}
