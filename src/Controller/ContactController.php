<?php

namespace App\Controller;

use DateTimeImmutable;
use App\Entity\Contact;
use App\Repository\ContactRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    #[Route('/contact', name: 'app_contact')]
    public function index(): Response
    {
        return $this->render('contact/index.html.twig');
    }

    #[Route('/validate/contact', name: 'app_validate_contact', methods: ['POST'])]
    public function validationContact(
        Request $request,
        ValidatorInterface $validator,
        ContactRepository $contactRepository,
        MailerInterface $mailer
    ): JsonResponse {
        $data = $request->toArray();

        $contact = new Contact();
        $contact
            ->setFirstname($data['firstname'])
            ->setLastname($data['lastname'])
            ->setPhone($data['phone'])
            ->setEmail($data['email'])
            ->setMessage($data['message'])
            ->setCreatedAt(new DateTimeImmutable());

        $validationErrors = $validator->validate($contact);

        $formErrors = [];
        if (count($validationErrors) > 0) {
            $formErrors['validationErrors'] = 'Les données soumises sont invalides';
            return new JsonResponse(['validationErrors' => $formErrors]);
        }

        $contactRepository->save($contact, true);

        $firstname = $data['firstname'];
        $lastname = $data['lastname'];
        $email = (new TemplatedEmail())
            ->from('hello@example.com')
            ->to('you@example.com')
            ->subject("Le café | Message de $firstname " . "$lastname")
            ->htmlTemplate('contact/email.html.twig')
            ->context([
                'contact' => $contact
            ]);
            
        $mailer->send($email);

        $this->addFlash('success', 'Merci ! Message bien reçu !');

        return new JsonResponse(['ok' => 'ok']);
    }
}
