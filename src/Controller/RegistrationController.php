<?php

namespace App\Controller;

use App\Entity\User;
use App\Security\EmailVerifier;
use App\Repository\UserRepository;
use Symfony\Component\Mime\Address;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

class RegistrationController extends AbstractController
{
    private EmailVerifier $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }

    #[Route('/inscription', name: 'app_register')]
    public function register(): Response
    {
        return $this->render('registration/register.html.twig');
    }

    #[Route('/validate/user', name: 'app_validate_user', methods: ['POST'])]
    public function validationUser(
        Request $request,
        UserPasswordHasherInterface $userPasswordHasher,
        ValidatorInterface $validator,
        UserRepository $userRepository
    ): JsonResponse {
        $data = $request->toArray();

        $user = new User();
        $user
            ->setEmail($data['email'])
            ->setPassword($data['plainPassword'])
            ->setIsVerified(false)
            ->setRoles(["ROLE_USER"]);

        $rgpd = $data['rgpd'];

        $emailError = $validator->validateProperty($user, 'email');
        $passwordError = $validator->validateProperty($user, 'password');
        $formErrors = [];
        if (count($emailError) > 0) {
            $formErrors['emailError'] = 'Email invalide';
        }
        if (count($passwordError) > 0) {
            $formErrors['passwordError'] = 'Mot de passe invalide';
        }
        if ($rgpd !== true) {
            $formErrors['rgpdError'] = 'Cette option est obligatoire';
        }
        if ($userRepository->findOneBy(['email' => $data['email']])) {
            $formErrors['existingUser'] = 'Cette adresse email est déjà utilisée !';
        }
        if ($formErrors) {
            return new JsonResponse(['formErrors' => $formErrors]);
        }

        $user->setPassword(
            $userPasswordHasher->hashPassword(
                $user,
                $data['plainPassword']
            )
        );

        $userRepository->save($user, true);

        // generate a signed url and email it to the user
        $this->emailVerifier->sendEmailConfirmation(
            'app_verify_email',
            $user,
            (new TemplatedEmail())
                ->from(new Address('adrien-le-pober@le-cafe.adrien-le-pober.com', 'Le café'))
                ->to($user->getEmail())
                ->subject('Confirmez votre adresse email')
                ->htmlTemplate('registration/confirmation_email.html.twig')
        );

        $this->addFlash('success', 'Votre compte a bien été créé, vous venez de recevoir un mail de confirmation');

        return new JsonResponse(['ok' => 'ok']);
    }

    #[Route('/verify/email', name: 'app_verify_email')]
    public function verifyUserEmail(
        Request $request,
        TranslatorInterface $translator,
        UserRepository $userRepository,
        ManagerRegistry $doctrine
    ): Response {
        $id = $request->get('id');

        if (null === $id) {
            return $this->redirectToRoute('app_register');
        }

        $user = $userRepository->find($id);

        if (null === $user) {
            return $this->redirectToRoute('app_register');
        }

        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $user);

            $entityManager = $doctrine->getManager();
            $user->setIsVerified(true);
            $entityManager->persist($user);
            $entityManager->flush();
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $translator->trans($exception->getReason(), [], 'VerifyEmailBundle'));

            return $this->redirectToRoute('app_register');
        }

        // @TODO Change the redirect on success and handle or remove the flash message in your templates
        $this->addFlash('success', 'Merci ! votre adresse email a bien été vérifiée !');

        return $this->redirectToRoute('app_login');
    }
}
