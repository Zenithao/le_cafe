<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    #[Route('/sitemap.xml', name: 'app_sitemap', defaults: ["_format"=>"xml"])]
    public function index(
        Request $request,
        UserRepository $userRepository
    ): Response {
        $hostname = $request->getSchemeAndHttpHost();

        $urls = [];
        $urls[] = ['loc' => $this->generateUrl('app_home')];
        $urls[] = ['loc' => $this->generateUrl('app_about')];
        $urls[] = ['loc' => $this->generateUrl('app_coffees')];
        $urls[] = ['loc' => $this->generateUrl('app_my_coffees')];
        $urls[] = ['loc' => $this->generateUrl('app_contact')];
        $urls[] = ['loc' => $this->generateUrl('app_login')];
        $urls[] = ['loc' => $this->generateUrl('app_register')];
        $urls[] = ['loc' => $this->generateUrl('app_forgot_password_request')];
        foreach ($userRepository->findAll() as $user) {
            $urls[] = ['loc' => $this->generateUrl('app_user_edit', ['id' => $user->getId()])];
        }

        $response = new Response(
            $this->renderView('sitemap/index.html.twig', [
                    'urls' => $urls,
                    'hostname' => $request->getSchemeAndHttpHost()
            ]),
            200
        );
        $response->headers->set('Content-type', 'text/xml');

        return $response;
    }
}
