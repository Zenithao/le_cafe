# Présentation

Le café est un projet personnel visant à présenter et partager mes connaissances.

# Fonctionnalités

# Installation

- Renseigner DATABASE_URL et MAILER_DSN dans le fichier .env
- Créer la base de données `symfony console d:d:c`
- Lancer les migrations `symfony console d:m:m`

# Lancer les tests

- `php bin/phpunit --testdox`