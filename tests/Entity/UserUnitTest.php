<?php

namespace App\Tests;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserUnitTest extends KernelTestCase
{
    public function getEntity(): User
	{
		return (new User())
            ->setEmail('true@test.com')
            ->setPassword('password')
            ->setUsername('user')
            ->setRoles(['ROLE_USER'])
            ->setFavorites(['1'])
            ->setIsVerified(true);
	}

    public function testIsTrue(): void
    {
        $this->assertTrue($this->getEntity()->getEmail() === 'true@test.com');
        $this->assertTrue($this->getEntity()->getUserIdentifier() === 'true@test.com');
        $this->assertTrue($this->getEntity()->getPassword() === 'password');
        $this->assertTrue($this->getEntity()->getUsername() === 'user');
        $this->assertTrue($this->getEntity()->getRoles() === ['ROLE_USER']);
        $this->assertTrue($this->getEntity()->getFavorites() === ["1"]);
        $this->assertTrue($this->getEntity()->isVerified() === true);
    }

    public function testIsFalse(): void
    {
        $this->assertFalse($this->getEntity()->getEmail() === 'false@test.com');
        $this->assertFalse($this->getEntity()->getUserIdentifier() === 'false@test.com');
        $this->assertFalse($this->getEntity()->getPassword() === 'false');
        $this->assertFalse($this->getEntity()->getUsername() === 'false');
        $this->assertFalse($this->getEntity()->getFavorites() === ["false"]);
        $this->assertFalse($this->getEntity()->isVerified() === false);
    }

    public function testIsEmpty(): void
    {
        $user = new User();

        $this->assertEmpty($user->getId());
        $this->assertEmpty($user->getUserIdentifier());
        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getUsername());
        $this->assertEmpty($user->getFavorites());
        $this->assertEmpty($user->isVerified());
    }

    // Tests des validateurs

    public function assertHasErrors(User $user, int $number = 0)
	{
		self::bootKernel();
		$container = static::getContainer();
		$errors = $container->get('validator')->validate($user);
		$messages = [];
		/** @var ConstraintViolation $error */
		foreach($errors as $error) {
			$messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
		}
		$this->assertCount($number, $errors, implode(', ', $messages));
	}

    public function testEntityIsValid()
	{
		$this->assertHasErrors($this->getEntity());
	}

    public function testInvalidBlank()
	{
		$this->assertHasErrors($this->getEntity()->setEmail(''), 1);
        $this->assertHasErrors($this->getEntity()->setPassword(''), 1);
	}

    public function testInvalidLength()
    {
        $this->assertHasErrors($this->getEntity()->setEmail(str_repeat('a', 181) . '@test.com'), 1);
        $this->assertHasErrors($this->getEntity()->setPassword(str_repeat('a', 101)), 1);
        $this->assertHasErrors($this->getEntity()->setUsername(str_repeat('a', 51)), 1);
    }

    public function testInvalidEmail()
    {
        $this->assertHasErrors($this->getEntity()->setEmail('a'), 1);
    }
}
