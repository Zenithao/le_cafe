<?php

namespace App\Tests;

use App\Entity\Contact;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ContactUnitTest extends KernelTestCase
{
    private DateTimeImmutable $date;

    public function getEntity(): Contact
	{
        $this->date = new DateTimeImmutable();

		return (new Contact())
            ->setFirstname('John')
            ->setLastname('Doe')
            ->setPhone('0160125585')
            ->setEmail('John@Doe.com')
            ->setMessage('Ceci est un message de test')
            ->setCreatedAt($this->date);
	}

    public function testIsTrue(): void
    {
        $this->assertTrue($this->getEntity()->getFirstname() === 'John');
        $this->assertTrue($this->getEntity()->getLastname() === 'Doe');
        $this->assertTrue($this->getEntity()->getPhone() === '0160125585');
        $this->assertTrue($this->getEntity()->getEmail() === 'John@Doe.com');
        $this->assertTrue($this->getEntity()->getMessage() === 'Ceci est un message de test');
        $this->assertTrue($this->getEntity()->getCreatedAt() === $this->date);
    }

    public function testIsFalse(): void
    {
        $this->assertFalse($this->getEntity()->getFirstname() === 'Francis');
        $this->assertFalse($this->getEntity()->getLastname() === 'Lalane');
        $this->assertFalse($this->getEntity()->getPhone() === '0260125586');
        $this->assertFalse($this->getEntity()->getEmail() === 'Francis@Lalane.com');
        $this->assertFalse($this->getEntity()->getMessage() === 'Ceci est un faux message de test');
        $this->assertFalse($this->getEntity()->getCreatedAt() === new DateTimeImmutable());
    }

    public function testIsEmpty(): void
    {
        $contact = new Contact();

        $this->assertEmpty($contact->getId());
        $this->assertEmpty($contact->getFirstname());
        $this->assertEmpty($contact->getLastname());
        $this->assertEmpty($contact->getPhone());
        $this->assertEmpty($contact->getEmail());
        $this->assertEmpty($contact->getMessage());
        $this->assertEmpty($contact->getCreatedAt());
    }

    // Tests des validateurs

    public function assertHasErrors(Contact $contact, int $number = 0)
	{
		self::bootKernel();
		$container = static::getContainer();
		$errors = $container->get('validator')->validate($contact);
		$messages = [];
		/** @var ConstraintViolation $error */
		foreach($errors as $error) {
			$messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
		}
		$this->assertCount($number, $errors, implode(', ', $messages));
	}

    public function testEntityIsValid()
	{
		$this->assertHasErrors($this->getEntity());
	}

    public function testInvalidBlank()
	{
		$this->assertHasErrors($this->getEntity()->setFirstname(''), 1);
        $this->assertHasErrors($this->getEntity()->setMessage(''), 1);
	}

    public function testInvalidLength()
    {
        $this->assertHasErrors($this->getEntity()->setFirstname(str_repeat('a', 51)), 1);
        $this->assertHasErrors($this->getEntity()->setLastname(str_repeat('a', 51)), 1);
        $this->assertHasErrors($this->getEntity()->setPhone(str_repeat('a', 51)), 1);
        $this->assertHasErrors($this->getEntity()->setEmail(str_repeat('a', 256) . '@test.com'), 1);
        $this->assertHasErrors($this->getEntity()->setMessage(str_repeat('a', 10001)), 1);
    }

    public function testInvalidEmail()
    {
        $this->assertHasErrors($this->getEntity()->setEmail('a'), 1);
    }
}
