<?php

namespace App\Tests;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CoffeeControllerTest extends WebTestCase
{
    public function testCoffeeList(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/les-cafes');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Liste des cafés');
    }

    public function testMyCoffeeList(): void
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('user@test.com');

        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/mes-cafes');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Mes favoris');
    }
}
