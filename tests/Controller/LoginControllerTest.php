<?php

namespace App\Tests;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginControllerTest extends WebTestCase
{
    public function testDisplayLogin(): void
    {
        $client = static::createClient();
        /** @var UrlGeneratorInterface $urlGenerator */
        $urlGenerator = $client->getContainer()->get("router");
        $crawler = $client->request('GET', $urlGenerator->generate('app_login'));

		$this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Connexion');

        $button = $crawler->selectButton('Valider');
        $this->assertEquals(1, count($button));
    }

    public function testSuccessfullLogin(): void
    {
        $client = static::createClient();
        /** @var UrlGeneratorInterface $urlGenerator */
        $urlGenerator = $client->getContainer()->get("router");
        $crawler = $client->request('GET', $urlGenerator->generate('app_login'));

        $form = $crawler->selectButton('Valider')->form([
            '_username' => 'user@test.com',
            '_password' => 'password'
        ]);

        $client->submit($form);

        // On vérifie qu'il y a bien une redirection (code 302)
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
    }

    public function testLoginWithBadCredentials()
    {
        $client = static::createClient();
        /** @var UrlGeneratorInterface $urlGenerator */
        $urlGenerator = $client->getContainer()->get("router");
        $crawler = $client->request('GET', $urlGenerator->generate('app_login'));

        $form = $crawler->selectButton('Valider')->form([
            '_username' => 'john@doe.fr',
            '_password' => 'fakepassword'
        ]);

        $client->submit($form);

        // On vérifie qu'il y a bien une redirection (code 302)
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);

        $client->followRedirect();

        $this->assertRouteSame('app_login');

        $this->assertSelectorExists('.text-danger', 'Identifiants invalides');
    }
}
