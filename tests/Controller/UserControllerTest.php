<?php

namespace App\Tests;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    public function testEditIsSuccessfull(): void
    {
        $client = static::createClient();
        /** @var UrlGeneratorInterface $urlGenerator */
		$urlGenerator = $client->getContainer()->get('router');
        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('user@test.com');

        $client->loginUser($testUser);

        $crawler = $client->request(
            'GET',
            $urlGenerator->generate('app_user_edit', ['id' => $testUser->getId()])
        );
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Mes informations personnelles');

        $form = $crawler->filter('form[name=user]')->form([
            'user[email]' => 'user@test.com'
        ]);

        $client->submit($form);
        $this->assertResponseStatusCodeSame(Response::HTTP_SEE_OTHER);
        $client->followRedirect();
        $this->assertSelectorTextContains('div.alert-success', 'Votre adresse email a bien été modifiée !');
        $this->assertRouteSame('app_user_edit', ['id' => $testUser->getId()]);
    }

    public function testDeleteIsSuccessfull(): void
    {
        $client = static::createClient();
        /** @var UrlGeneratorInterface $urlGenerator */
		$urlGenerator = $client->getContainer()->get('router');
        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('user@test.com');

        $client->loginUser($testUser);
        $crawler = $client->request(
            'POST',
            $urlGenerator->generate('app_user_delete', ['id' => $testUser->getId()])
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_SEE_OTHER);
        $client->followRedirect();
        $this->assertRouteSame('app_login');
    }
}
